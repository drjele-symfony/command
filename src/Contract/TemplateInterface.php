<?php

declare(strict_types=1);

/*
 * Copyright (c) Adrian Jeledintan
 */

namespace Drjele\Symfony\Command\Contract;

use Drjele\Symfony\Command\Dto\ConfFilesDto;

interface TemplateInterface
{
    public function generate(ConfigInterface $configDto, array $commands): ConfFilesDto;
}
