<?php

declare(strict_types=1);

/*
 * Copyright (c) Adrian Jeledintan
 */

namespace Drjele\Symfony\Command\Command;

use Drjele\Symfony\Command\Dto\Cron\CronDto;
use Drjele\Symfony\Command\Service\ConfGenerateService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

class CronCreateCommand extends AbstractCommand
{
    public const NAME = 'drjele:symfony:command:cron-create';

    private ConfGenerateService $confGenerateService;
    private ?CronDto $cronDto;

    public function __construct(
        ConfGenerateService $confGenerateService,
        ?array $config
    ) {
        $this->confGenerateService = $confGenerateService;
        $this->cronDto = null === $config ? null : new CronDto($config);

        parent::__construct(static::NAME);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (null === $this->cronDto) {
            $this->warning('no configuration is set');

            return static::SUCCESS;
        }

        try {
            $confFiles = $this->confGenerateService->generate(
                $this->cronDto->getConfig(),
                $this->cronDto->getCommands()
            );

            $confFilesCount = \count($confFiles);

            if (0 === $confFilesCount) {
                $this->warning('no conf files were generated');
            } else {
                $this->success(\sprintf('generated `%s` conf files', $confFilesCount));

                foreach ($confFiles as $confFile) {
                    $this->writeln($confFile);
                }
            }
        } catch (Throwable $e) {
            $this->error($e->getMessage());

            return static::FAILURE;
        }

        return static::SUCCESS;
    }
}
