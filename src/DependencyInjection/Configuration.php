<?php

declare(strict_types=1);

/*
 * Copyright (c) Adrian Jeledintan
 */

namespace Drjele\Symfony\Command\DependencyInjection;

use Drjele\Symfony\Command\Template\CrontabTemplate;
use Drjele\Symfony\Command\Template\SupervisorTemplate;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeBuilder;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public const COMMAND = 'command';
    public const SCHEDULE = 'schedule';
    public const LOG = 'log';
    public const TEMPLATE_CLASS = 'template_class';
    public const CONF_FILES_DIR = 'conf_files_dir';
    public const LOGS_DIR = 'logs_dir';
    public const HEARTBEAT = 'heartbeat';
    public const DESTINATION_FILE = 'destination_file';
    public const CONFIG = 'config';
    public const COMMANDS = 'commands';
    public const MINUTE = 'minute';
    public const HOUR = 'hour';
    public const DAY_OF_MONTH = 'day_of_month';
    public const MONTH = 'month';
    public const DAY_OF_WEEK = 'day_of_week';
    public const NUMBER_OF_PROCESSES = 'number_of_processes';
    public const AUTO_START = 'auto_start';
    public const AUTO_RESTART = 'auto_restart';
    public const PREFIX = 'prefix';
    public const USER = 'user';
    public const CRON = 'cron';
    public const WORKER = 'worker';
    public const SETTINGS = 'settings';

    private const DESTINATION_DIR = '%kernel.project_dir%/generated_conf/';
    private const NAME = 'name';

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('drjele_symfony_command');

        $treeBuilder->getRootNode()
            ->children()
            ->append($this->buildCron())
            ->append($this->buildWorker());

        return $treeBuilder;
    }

    private function buildCron(): NodeDefinition
    {
        $cronTree = (new TreeBuilder(static::CRON))->getRootNode()
            ->addDefaultsIfNotSet();

        $cronConfigTree = $cronTree->children()->arrayNode(static::CONFIG)
            ->addDefaultsIfNotSet();

        $cronConfigTree->children()
            ->scalarNode(static::TEMPLATE_CLASS)->defaultValue(CrontabTemplate::class)->end()
            ->scalarNode(static::CONF_FILES_DIR)->defaultValue(static::DESTINATION_DIR . 'cron')->end()
            ->scalarNode(static::LOGS_DIR)->defaultValue('%kernel.logs_dir%/cron')->end();

        $cronConfigTree->children()->arrayNode(static::SETTINGS)
            ->ignoreExtraKeys(false)
            ->addDefaultsIfNotSet()
            ->children()
            ->booleanNode(static::LOG)->defaultTrue()->end()
            ->scalarNode(static::DESTINATION_FILE)->defaultValue('crontab')->end()
            ->booleanNode(static::HEARTBEAT)->defaultTrue()->end();

        /** @var NodeBuilder|ArrayNodeDefinition $cronCommandsTree */
        $cronCommandsTree = $cronTree->children()->arrayNode(static::COMMANDS)
            ->isRequired()
            ->useAttributeAsKey(static::NAME)
            ->prototype('array');

        $cronCommandsTree->children()
            ->scalarNode(static::NAME)->end()
            ->arrayNode(static::COMMAND)/* command start */
            ->isRequired()
            ->beforeNormalization()->ifString()->then(fn ($command) => [$command])->end()
            ->scalarPrototype()->end()
            ->end()/* command end */
            ->arrayNode(static::SCHEDULE)/* schedule start */
            ->children()
            ->scalarNode(static::MINUTE)->defaultValue('*')->end()
            ->scalarNode(static::HOUR)->defaultValue('*')->end()
            ->scalarNode(static::DAY_OF_MONTH)->defaultValue('*')->end()
            ->scalarNode(static::MONTH)->defaultValue('*')->end()
            ->scalarNode(static::DAY_OF_WEEK)->defaultValue('*')->end()
            ->end()
            ->end(); /* schedule end */

        $cronCommandsTree->children()->arrayNode(static::SETTINGS)
            ->ignoreExtraKeys(false)
            ->addDefaultsIfNotSet()
            ->children()
            ->booleanNode(static::LOG)->defaultNull()->end();

        return $cronTree;
    }

    private function buildWorker(): NodeDefinition
    {
        $workerTree = (new TreeBuilder(static::WORKER))->getRootNode()
            ->addDefaultsIfNotSet();

        $workerConfigTree = $workerTree->children()->arrayNode(static::CONFIG)
            ->addDefaultsIfNotSet();

        $workerConfigTree->children()
            ->scalarNode(static::TEMPLATE_CLASS)->defaultValue(SupervisorTemplate::class)->end()
            ->scalarNode(static::CONF_FILES_DIR)->defaultValue(static::DESTINATION_DIR . 'worker')->end()
            ->scalarNode(static::LOGS_DIR)->defaultValue('%kernel.logs_dir%/worker')->end();

        $settingsTree = $workerConfigTree->children()->arrayNode(static::SETTINGS)
            ->ignoreExtraKeys(false)
            ->addDefaultsIfNotSet();
        $this->appendSupervisorConfig($settingsTree->children());

        /** @var NodeBuilder|ArrayNodeDefinition $workerCommandsTree */
        $workerCommandsTree = $workerTree->children()->arrayNode(static::COMMANDS)
            ->isRequired()
            ->useAttributeAsKey(static::NAME)
            ->prototype('array');

        $workerCommandsTree->children()
            ->scalarNode(static::NAME)->end()
            ->arrayNode(static::COMMAND)
            ->isRequired()
            ->beforeNormalization()->ifString()->then(fn ($command) => [$command])->end()
            ->scalarPrototype()->end()
            ->end();

        $settingsTree = $workerCommandsTree->children()->arrayNode(static::SETTINGS)
            ->ignoreExtraKeys(false)
            ->addDefaultsIfNotSet();
        $this->appendSupervisorConfig($settingsTree->children());

        return $workerTree;
    }

    private function appendSupervisorConfig(NodeBuilder $node): self
    {
        $node->integerNode(static::NUMBER_OF_PROCESSES)->defaultValue(1)->end()
            ->booleanNode(static::AUTO_START)->defaultTrue()->end()
            ->booleanNode(static::AUTO_RESTART)->defaultTrue()->end()
            ->scalarNode(static::PREFIX)->defaultNull()->end()
            ->scalarNode(static::USER)->defaultNull()->end();

        return $this;
    }
}
