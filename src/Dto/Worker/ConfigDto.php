<?php

declare(strict_types=1);

/*
 * Copyright (c) Adrian Jeledintan
 */

namespace Drjele\Symfony\Command\Dto\Worker;

use Drjele\Symfony\Command\Contract\ConfigInterface;
use Drjele\Symfony\Command\DependencyInjection\Configuration;
use Drjele\Symfony\Command\Dto\Traits\ConfigTrait;

class ConfigDto implements ConfigInterface
{
    use ConfigTrait;

    private ConfigSettingsDto $settings;

    public function __construct(array $config)
    {
        $this->setConfigs($config);

        $this->settings = new ConfigSettingsDto($config[Configuration::SETTINGS]);
    }

    public function getSettings(): ConfigSettingsDto
    {
        return $this->settings;
    }
}
