<?php

declare(strict_types=1);

/*
 * Copyright (c) Adrian Jeledintan
 */

namespace Drjele\Symfony\Command\Dto\Worker;

use Drjele\Symfony\Command\Dto\Traits\SettingsTrait;
use Drjele\Symfony\Command\Dto\Traits\SupervisorSettingsTrait;

class CommandSettingsDto
{
    use SettingsTrait;
    use SupervisorSettingsTrait;

    public function __construct(array $settings)
    {
        $this->initSupervisorSettings();

        $this->loadProperties($settings);
    }
}
