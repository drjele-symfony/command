<?php

declare(strict_types=1);

/*
 * Copyright (c) Adrian Jeledintan
 */

namespace Drjele\Symfony\Command\Dto\Cron;

use Drjele\Symfony\Command\Dto\Traits\SettingsTrait;

class CommandSettingsDto
{
    use SettingsTrait;

    private ?bool $log;

    public function __construct(array $settings)
    {
        $this->log = null;

        $this->loadProperties($settings);
    }

    public function getLog(): ?bool
    {
        return $this->log;
    }
}
