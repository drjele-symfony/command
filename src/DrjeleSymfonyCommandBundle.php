<?php

declare(strict_types=1);

/*
 * Copyright (c) Adrian Jeledintan
 */

namespace Drjele\Symfony\Command;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class DrjeleSymfonyCommandBundle extends Bundle
{
}
