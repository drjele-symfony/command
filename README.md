**THIS PROJECT IS NO LONGER MAINTAINED, THE NEW PROJECT IS https://gitlab.com/drjele-symfony/console**

# Symfony command

**You may fork and modify it as you wish**.

Any suggestions are welcomed.

## Config example

**drjele_symfony_command.yaml**

```yaml
drjele_symfony_command:
    cron:
        config:
            template_class: \Drjele\Symfony\Command\Template\CrontabTemplate
            conf_files_dir: '%kernel.project_dir%/generated_conf/cron'
            logs_dir: '%kernel.logs_dir%/cron'
            log: true
            destination_file: 'crontab'
            heartbeat: true
        commands:
            list:
                command: '%kernel.project_dir%/bin/console list'
                schedule:
                    minute: '*'
                    hour: '*'
                    day_of_month: '*'
                    month: '*'
                    day_of_week: '*'
                log: false

    worker:
        config:
            template_class: \Drjele\Symfony\Command\Template\SupervisorTemplate
            conf_files_dir: '%kernel.project_dir%/generated_conf/worker'
            logs_dir: '%kernel.logs_dir%/worker'
            number_of_processes: 1
            auto_start: true
            auto_restart: true
            prefix: 'app-name'
            user: 'root'
        commands:
            list:
                command: '%kernel.project_dir%/bin/console list'
                number_of_processes: 2
```

If the **drjele_symfony_command.cron.config.heartbeat** a command with the name `heartbeat` will automatically be added, if not added by you.

## For custom templates

Create a template service to implement **Drjele\Symfony\Command\Contract\TemplateInterface** and add to your **services.yaml** this config:

```yaml
services:
    _instanceof:
        Drjele\Symfony\Command\Contract\TemplateInterface:
            tags: ['drjele.symfony.command.template']
```

## Dev

* `git clone git@gitlab.com:drjele-symfony/command.git`
* `cd command/scripts/docker/`
* `cp ~/.ssh/id_* ./`
* `create .profile_personal with:`

```
echo 'git config --global user.name "<your name>"' >> ./.profile_personal && \
echo 'git config --global user.email "<your email>"' >> ./.profile_personal
```

* `docker-compose build && docker-compose up -d`
* `docker-compose exec php sh`
* `composer install`
